import React from "react";
import "./Partners.css";

const Partners = (props) => {
    const partners = props.partners.map((partner) => (
        <div className="Partner" key={partner}>
            <img src={partner} alt="partners" />
        </div>
    ));

    return (
        <div className="PartnersBlock">
            <h3>Нам Доверяют</h3>
            <p>Больше половины наших клиент международные компании и организации</p>
            <div className="Partners">{partners}</div>
        </div>
    );
};

export default Partners;
