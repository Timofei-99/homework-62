import React from "react";
import "./MainBlock.css";

const MainBlock = () => {
    return (
        <div className="MainBlock">
            <img
                src="https://pngimg.com/uploads/amazon/amazon_PNG9.png"
                alt="amazon"
            />
            <h1>Добро Пожаловать в Наш Прекраснный Коллектив</h1>
            <p>
                Наша Компания уже более 10 лет на рынке. Мы предлагаем вам лучший сервис
                в стране Быстрая и качественная доставка в любое точку Мира
            </p>
        </div>
    );
};

export default MainBlock;
