import React from "react";
import "./Footer.css";

const Footer = () => {
    return (
        <footer className="Footer">
            <div className="Container">
                <p>2021 tima9953@gmail.com</p>
            </div>
        </footer>
    );
};

export default Footer;
