import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import Footer from "./Components/UI/Footer/Footer";
import Header from "./Components/UI/Header/Header";
import CompanyPage from "./Container/CompanyPage/CompanyPage";
import FeedBack from "./Container/Feedback/Feedback";
import MainPage from "./Container/MainPage/MainPage";
import Shares from "./Container/Shares/Shares";

const App = () => (
    <BrowserRouter>
      <Header />
      <div className="Container">
        <Switch>
          <Route path="/" exact component={MainPage} />
          <Route path="/feedback" component={FeedBack} />
          <Route path="/company" component={CompanyPage} />
          <Route path="/shares" component={Shares} />
        </Switch>
      </div>
      <Footer />
    </BrowserRouter>
);

export default App;
