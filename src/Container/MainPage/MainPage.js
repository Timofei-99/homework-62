import React, { useState } from "react";
import AboutUS from "../../Components/AboutUs/AboutUs";
import Branch from "../../Components/Branch/Branch";
import MainBlock from "../../Components/MainBlock/MainBlock";
import Partners from "../../Components/Partners/Partners";
import "./MainPage.css";

const MainPage = (props) => {
    const [branches] = useState([
        {
            country: "Кыргызстан",
            phoneNumber: "+996-550-11-74-17",
            address: "Адрес: ул.Советская дом 17",
        },
        {
            country: "Казахстан",
            phoneNumber: "+8710337345",
            address: "Адрес: ул.Советская дом 17",
        },
        {
            country: "Россия",
            phoneNumber: "+74232497777",
            address: "Адрес: ул.Советская дом 17",
        },
    ]);
    const [partners] = useState([
        "https://www.logobank.ru/images/ph/en/c/coca-cola.png",
        "https://video.o.kg/image/logo.png",
        "https://upload.wikimedia.org/wikipedia/commons/7/7a/BeeLine_logo.png",
        "https://upload.wikimedia.org/wikipedia/commons/c/c0/Logotip_OPTIMA-BANK.jpg",
    ]);

    const branchesBlock = branches.map((branche) => (
        <Branch
            key={branche.country}
            country={branche.country}
            phone={branche.phoneNumber}
            address={branche.address}
        />
    ));
    const purchaseContinuedHandler = () => {
        props.history.push({
            pathname: "/feedback",
        });
    };

    return (
        <>
            <MainBlock />
            <AboutUS />
            <Partners partners={partners} />
            <div className="Branches">
                {branchesBlock}
                <button onClick={purchaseContinuedHandler} className="connection">
                    Заказать Обратную Связь
                </button>
            </div>
        </>
    );
};

export default MainPage;
