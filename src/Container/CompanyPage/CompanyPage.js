import React, { useState } from "react";
import Position from "../../Components/Position/Position";
import "./CompanyPage.css";

const CompanyPage = () => {
    const [persons] = useState([
        {
            name: "Василий Петров",
            position: "Председатель правления",
            text:
                "Чтобы качественно предоставлять услуги мало иметь правильное понимание бизнеса, важно уметь думать вне своих рамок. Важно научиться предвидеть новые возможности, идти на риск, развиваться и принимать новые вызовы! Нет ничего не возможного, нужно найти грамотное решение! Наш коллектив состоит исключительно из таких людей, которые понимают этот принцип! С нашей командой, мы воплощаем идеи наших многочисленных клиентов",
        },
        {
            name: "Василий Петров",
            position: "Начальник отдела",
            text:
                "Наши достижения – это трудоемкая и упорная работа! Ежедневно нам приходится решать сложные задачи, связанные с грузоперевозками. Порой бизнес нашего клиента зависит именно от нас, от того насколько грамотно мы осуществим свою работу. Мы найдем решение даже для самого сложного проекта.",
        },
        {
            name: "Василий Петров",
            position: "Супервайзер",
            text:
                "Наши достижения – это трудоемкая и упорная работа! Ежедневно нам приходится решать сложные задачи, связанные с грузоперевозками. Порой бизнес нашего клиента зависит именно от нас, от того насколько грамотно мы осуществим свою работу. Мы найдем решение даже для самого сложного проекта.",
        },
    ]);

    const person = persons.map((person) => (
        <Position
            key={person.position}
            name={person.name}
            position={person.position}
            text={person.text}
        />
    ));
    return (
        <>
            <div>
                <h2> МЫ ЯВЛЯЕМСЯ УЧАСТНИКОМ МЕЖДУНАРОДНОЙ ОРГАНИЗАЦИИ В ЛОГИСТИКЕ</h2>
                <p>
                    Наше успешное и стремительное развитие помогло нам стать участниками в
                    международных транспортных и логистических организациях. В эти
                    организации могут вступить только лучшие представители своей отрасли
                </p>
            </div>
            <div>
                <div className="Positions">
                    <h3>НАШЕ ПРАВЛЕНИЕ</h3>
                    {person}
                </div>
            </div>
        </>
    );
};

export default CompanyPage;
