import React from "react";
import "./Shares.css";

const Shares = () => {
    return (
        <div className="Shares">
            <h5>В этом разделе Вы можете отслеживать Прохoдящие акции</h5>
            <div>
                <div>
                    <p>В данное время у Нас нет подходящих Акций </p>
                </div>
                <ul>
                    Акции не действуют В случае:
                    <li>Если товар и так проходит по акции</li>
                    <li>Если товар имеет маленький срок годности</li>
                    <li>Если товар находился в использовании </li>
                    <li>А так же при заказе по сертификату</li>
                </ul>
            </div>
        </div>
    );
};

export default Shares;
