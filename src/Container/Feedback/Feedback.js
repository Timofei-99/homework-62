import React from "react";
import './FeedBack.css';

const Feedback = () => {
    const handlerEvent = () => {
        alert("Ожидайте Скоро Наш Оператор Вам перезвонит");
    };
    return (
        <div className="FeedBack">
            <img
                src="https://pngimg.com/uploads/amazon/amazon_PNG9.png"
                alt="amazon"
            />
            <p>
                Чтобы получить обратную связь Вам Необходимо заполнить следущие поле:
            </p>
            <form className="form">
                <label>
                    Фамилия
                    <input className="field" />
                </label>
                <label>
                    Имя
                    <input className="field" />
                </label>
                <label>
                    Номер Телефона
                    <input className="field" />
                </label>
                <button onClick={handlerEvent}>Заказать Звонок</button>
            </form>
        </div>
    );
};

export default Feedback;
